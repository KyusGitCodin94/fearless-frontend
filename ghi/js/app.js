function createCard(name, location, description, pictureUrl, start, end) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="card-subtitle mb-2 text-secondary">${location}</h6>
        <p class="card-text">${description}</p>
      </div>
      <div class="card-footer">
        ${start} - ${end}
      </div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {

    } else {
      const data = await response.json();

      // KS: counter to determine which column to place each conference card in
      let confArrayPos = -1;

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {

          confArrayPos += 1;

          const details = await detailResponse.json();
          //console.log(details);
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;

          let startDate = new Date(details.conference.starts);
          startDate = startDate.toLocaleDateString();

          let endDate = new Date(details.conference.ends);
          endDate = endDate.toLocaleDateString();

          const location = details.conference.location.name;

          const html = createCard(title, location, description, pictureUrl, startDate, endDate);
          const column = document.querySelectorAll('.col')[confArrayPos%3];
          column.innerHTML += html;
        }
      }
    }
  } catch (e) {
    console.error(e);
  }

});
