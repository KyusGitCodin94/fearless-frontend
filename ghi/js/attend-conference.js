window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    //console.log(data);

    let options = document.getElementById('conference');

    for (let conf of data['conferences']) {

      let option = document.createElement('option');
      option.value = conf['href'];
      option.innerHTML = conf['name'];

      options.appendChild(option);

    }
  }

  const formTag = document.getElementById('create-attendee-form');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const locationUrl = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
    }
  });

});
