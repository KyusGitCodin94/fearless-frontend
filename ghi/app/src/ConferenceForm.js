import React, {useEffect, useState} from 'react';

function ConferenceForm (props) {

    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const [starts, setStarts] = useState('');

    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }

    const [ends, setEnds] = useState('');

    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }

    const [desc, setDesc] = useState('');

    const handleDescChange = (event) => {
        const value = event.target.value;
        setDesc(value);
    }

    const [maxPres, setMaxPres] = useState('');

    const handleMaxPresChange = (event) => {
        const value = event.target.value;
        setMaxPres(value);
    }

    const [maxAtts, setMaxAtts] = useState('');

    const handleMaxAttsChange = (event) => {
        const value = event.target.value;
        setMaxAtts(value);
    }

    const [loc, setLoc] = useState('');

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLoc(value);
    }

    const fetchData = async () => {

      const url = 'http://localhost:8000/api/locations/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        //console.log(data);
        setLocations(data.locations);
      }

    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {

      event.preventDefault();

      // create an empty JSON object
      const data = {};

      data.name = name;
      data.starts = starts;
      data.ends = ends;
      data.description = desc;
      data.max_presentations = maxPres;
      data.max_attendees = maxAtts;
      data.location = loc;

      console.log(data);

      const locationUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
      };

      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {

        const newLocation = await response.json();
        console.log(newLocation);

        setName('');
        setStarts('');
        setEnds('');
        setDesc('');
        setMaxPres('');
        setMaxAtts('');
        setLoc('');

      }

    }

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescChange} value={desc} className="form-control" id="description" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresChange} value={maxPres} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttsChange} value={maxAtts} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={loc} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.id} value={location.id}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );

}

export default ConferenceForm;
